
const express = require("express"); 
const bodyParser = require("body-parser"); 
const app = express(); 
let port = 8081; 
app.use(bodyParser.json()); 

app.listen(port, () => {

  console.log("Server open on: " + port);
});

app.get("/", (req, res) => {
  try {
    res
      .status(200)
      .send("Salut, sunt site-ul care pica. Ai grija sa ma prinzi.");
  } catch {
    res.status(404).send("Am picat. Ajutor.");
  }
});
